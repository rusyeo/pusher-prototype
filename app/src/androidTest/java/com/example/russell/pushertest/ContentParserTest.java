package com.example.russell.pushertest;

import android.support.test.runner.AndroidJUnit4;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;


import static org.junit.Assert.assertEquals;

/**
 *  This is in androidTest because JSONObject is an android library.
 */

@RunWith(AndroidJUnit4.class)
public class ContentParserTest {

    private ContentParser parser;

    @Before
    public void setUp() {
        parser = new ContentParser();
    }

    @Test
    public void encodeMessage() throws Exception {
        long time = System.currentTimeMillis();
        Message testMessage = new Message("Text", "Name", time);

        String expected = "{\"text\":\"Text\",\"name\":\"Name\",\"time\":" + time + "}";
        String result = parser.encodeMessage(testMessage);

        assertEquals(expected, result);
    }

    @Test
    public void decodeServerResponse() throws Exception {
        JSONObject testJSON = new JSONObject();
        String result = parser.decodeServerResponse(testJSON);
        assertEquals("{}", result);
    }

}