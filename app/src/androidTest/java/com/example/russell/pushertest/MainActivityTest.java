package com.example.russell.pushertest;

import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

import static org.mockito.Mockito.verify;
import static org.mockito.Matchers.any;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * Created by russell on 11/05/2017.
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class MainActivityTest {

//    public static final String jsonString = "{name = 'user', text = 'message', time = 12345}";

    @Rule public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    @Mock private IMessageSender client;
    @Mock private Message message;

    @Before
    public void setUp() throws Exception {
        initMocks(this);
    }

    @Test
    public void sendMessage_success() throws Exception {
        final MainActivity activity = mActivityRule.getActivity();
        activity.client = client;

        onView(
                withId(R.id.message_input)
        ).perform(
                typeText("Test Message 1234")
        );

        onView(
                withId(R.id.send_button)
        ).perform(
                click()
        );


        verify(client).sendMessage(any(Message.class), any(IMessageSender.MessageSenderCallback.class));

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                activity.callback.onSuccess("String");
            }
        });


    }

    @Test
    public void sendMessage_failure() throws Exception {
        final MainActivity activity = mActivityRule.getActivity();
        activity.client = client;


        onView(
                withId(R.id.message_input)
        ).perform(
                typeText("Message")
        );

        onView(
                withId(R.id.send_button)
        ).perform(
                click()
        );

        verify(client).sendMessage(any(Message.class), any(IMessageSender.MessageSenderCallback.class));

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                activity.callback.onError(404, "Not Found");
            }
        });

    }


}