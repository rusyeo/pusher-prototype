package com.example.russell.pushertest;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.pusher.client.Pusher;
import com.pusher.client.PusherOptions;
import com.pusher.client.channel.Channel;
import com.pusher.client.channel.SubscriptionEventListener;

import java.io.IOException;
import java.util.ArrayList;

public class MainActivity extends ActionBarActivity implements View.OnClickListener {

    EditText messageInput;
    Button sendButton;
    String username = "Russell";
    MessageAdapter messageAdapter;
    Message message;

    // Init MessageSender
    IMessageSender client = new MessageSender(this, new ContentParser());

    // Callback for the MessageSender
    IMessageSender.MessageSenderCallback callback = new IMessageSender.MessageSenderCallback() {
        @Override
        public void onSuccess(String serverResponse) {
            messageInput.setText("");
            System.out.println("" + serverResponse);
        }

        @Override
        public void onError(int statusCode, String message) {
            Toast.makeText(
                    getApplicationContext(),
                    "Something went wrong :(",
                    Toast.LENGTH_LONG
            ).show();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        messageInput = (EditText) findViewById(R.id.message_input);
        sendButton = (Button) findViewById(R.id.send_button);
        sendButton.setOnClickListener(this);

        // Bind the ListView to the Adapter
        messageAdapter = new MessageAdapter(this, new ArrayList<Message>());

        final ListView messagesView = (ListView) findViewById(R.id.messages_view);
        messagesView.setAdapter(messageAdapter);

        PusherOptions options = new PusherOptions();
        options.setCluster("eu");
        Pusher pusher = new Pusher("d32aba8e88b99096cecc", options);

        // subscribe to our "messages" channel
        Channel channel = pusher.subscribe("messages");

        // listen for the "new_message" event
        channel.bind("new_message", new SubscriptionEventListener() {
            @Override
            public void onEvent(String channelName, String eventName, final String data) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Gson gson = new Gson();
                        Message message = gson.fromJson(data, Message.class);
                        messageAdapter.add(message);

                        // have the ListView scroll down to the new message
                        messagesView.setSelection(messageAdapter.getCount() - 1);
                    }
                });
            }
        });

        // Connect to the Pusher API
        pusher.connect();
    }

    // Clicking send posts a message
    @Override
    public void onClick(View v) {
        try {
            postMessage();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void postMessage() throws IOException {
        String text = messageInput.getText().toString();
        // return if the text is blank
        if (text.equals("")) {
            return;
        }
        message = new Message(text, username);
        client.sendMessage(message, callback);
    }
}
