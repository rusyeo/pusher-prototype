package com.example.russell.pushertest;

import android.app.Activity;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONObject;

import java.io.IOException;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

/**
 * Created by russell on 11/05/2017.
 */

public class MessageSender implements IMessageSender {

    final String MESSAGES_ENDPOINT = "http://192.168.129.147:3000/pusher/messages";

    private final AsyncHttpClient httpClient;
    private final IContentParser parser;
    private Activity context;

    public MessageSender(Activity context, ContentParser messageParser) {
        this.context = context;
        httpClient = new AsyncHttpClient();
        parser = messageParser;
    }

    @Override
    public void sendMessage(Message message, final MessageSenderCallback callback) throws IOException {
        StringEntity entity = new StringEntity(parser.encodeMessage(message));
        System.out.println(entity.toString());

        httpClient.post(context, MESSAGES_ENDPOINT, entity, "application/json", new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                context.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        callback.onSuccess(parser.decodeServerResponse(response));
                    }
                });
            }

            @Override
            public void onFailure(final int statusCode, Header[] headers, final String responseString, Throwable throwable) {
                context.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        callback.onError(statusCode, responseString);
                    }
                });
            }
        });
    }
}
