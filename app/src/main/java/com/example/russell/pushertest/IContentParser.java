package com.example.russell.pushertest;

import org.json.JSONObject;

/**
 *  Content Parsing.
 *  Encode Message to a string ready to send to server.
 *  Decode Server response ready for handling.
 */

interface IContentParser {

    String encodeMessage(Message message);
    String decodeServerResponse(JSONObject response);

}
