package com.example.russell.pushertest;

import java.util.Date;

/**
 *  Message data object (immutable)
 */

public class Message {
    private final String text;
    private final String name;
    private final long time;

    public Message(String text, String username) {
        this(text, username, new Date().getTime());
    }

    public Message(String text, String username, long time) {
        this.text = text;
        this.name = username;
        this.time = time;
    }

    public String getText() {
        return text;
    }

    public String getName() {
        return name;
    }

    public long getTime() {
        return time;
    }
}
