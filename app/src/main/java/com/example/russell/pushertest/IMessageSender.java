package com.example.russell.pushertest;

import java.io.IOException;

/**
 *  Handle the sending of messages
 */

interface IMessageSender {

    void sendMessage(Message message, MessageSenderCallback callback) throws IOException;

    interface MessageSenderCallback {
        void onSuccess(String string);
        void onError(int statusCode, String message);

    }
}
