package com.example.russell.pushertest;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by russell on 11/05/2017.
 */

class ContentParser implements IContentParser {

    // Receive a Message, convert it to JSON then to String
    @Override
    public String encodeMessage(Message message) {
        JSONObject jsonParams = new JSONObject();
        try {
            jsonParams.put("text", message.getText());
            jsonParams.put("name", message.getName());
            jsonParams.put("time", message.getTime());
        } catch (JSONException e) {
            System.out.println("Error encoding Message");
        }
        return jsonParams.toString();
    }


    // TODO decide the content of the response body
    // Currently server always responds with "{}"
    @Override
    public String decodeServerResponse(JSONObject response) {
        String string = response.toString();
        return string;
    }

}
